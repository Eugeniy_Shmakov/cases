import os
import math as ma

home_dir = os.getcwd()
NUMBER_OF_PROC = 16
DC = "decomposePar -force"
RUN = f"mpirun -np {NUMBER_OF_PROC} mhdVxBPhasePimpleFoam -parallel : -np {NUMBER_OF_PROC} ElmerSolver_mpi | tee log -a"
FILES = ["case.sif", "constant/transportProperties", "0/U", "0/U_old"]


def hartmann_mfd(ha):
    lendth = 0.1 / 2
    sigma = 3.838e6
    nu = 9.645e-7
    rho = 1576
    mfd = ha / (lendth * (sigma / (nu * rho)) ** 0.5)
    mfs = mfd / (4e-7 * ma.pi)
    return mfs

    
def hartman(mfd):
    lendth = 0.1 / 2
    sigma = 3.838e6
    nu = 9.645e-7
    rho = 1576
    hartman = mfd * lendth * (sigma / (nu * rho)) ** 0.5
    return hartman
 
 
def stuart(mfd, velocity):
    stuart = hartman(mfd) ** 2 / reynolds(velocity)
    return stuart
    

def stuart_mfs(st, freq, slip):
    lendth = 0.1 / 2
    sigma = 3.838e6
    rho = 1576
    velocity = velocity_slip(freq, slip)
    mfd = (st * rho * velocity / (lendth * sigma)) ** 0.5
    mfs = mfd / (4e-7 * ma.pi)
    return mfs
    
    
def reynolds(velocity):
    nu = 9.645e-7
    lendth = 0.1 / 2
    reynolds = velocity * lendth / nu
    return reynolds


def set_param(var_name, function, file_name):
    command = f"sed -i 's/{var_name}/{function:.1f}/' {file_name}"
    os.system(command)
    
    
def calculation():
    os.system(DC)
    os.system(RUN)
    os.chdir(home_dir)
    
       
def velocity_slip(freq, slip):
    tau = 0.15
    v_sinh = 2 * tau * freq
    velocity = v_sinh * (1 - slip)
    return velocity
    

def make_init(freq):
    DIR = home_dir+f"/freq_{freq}"
    os.system(f"cp -r Base freq_{freq}")
    os.chdir(DIR)
    os.chdir(INITIAL)
    poly_0, poly_2 = polynom(velocity_slip(freq, 0.3))
    set_param("VAR_0", poly_0, FILES[2])
    set_param("VAR_2", poly_2, FILES[2])
    set_param("VAR_0", poly_0, FILES[3])
    set_param("VAR_2", poly_2, FILES[3])
    os.chdir(home_dir)
    
    
def make_calc(st):
    DIR = home_dir+f"/St_{st}"
    os.system(f"cp -r Base St_{st}")
    os.chdir(DIR)
    set_param("VAR", stuart_mfs(st, 50, 0.3), FILES[0])
    calculation()
    
    
def polynom(velocity):
    coef_2 = velocity / 0.0025
    coef_0 = velocity
    return coef_0, coef_2
    
    
def reconstruct_par(st):
    DIR = home_dir+f"/St_{st}"
    os.chdir(DIR)
    os.system("reconstructPar")
    os.chdir(home_dir)
    

def main():
    for st in range(1,6,2):
        make_calc(st)
    for st in range(20,60,10):
        make_calc(st)
    make_calc(100)
            
        
        
if __name__ == "__main__":
    main()
    
